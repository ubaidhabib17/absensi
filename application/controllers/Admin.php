<?php
defined('BASEPATH') or exit('No direct script access');
class Admin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		cek_login();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->model('menu_model');
		$this->load->model('presensi_model');
	}
	public function index()
	{
		$data['title'] = 'Dashboard';
		// $data['user'] = $this->db->get_where('user', ['email' =>
		// $this->session->userdata('email')])->row_array();
		
		$data['user'] = $this->db->get_where('sekolah', ['email' =>
		$this->session->userdata('email')])->row_array();
		$kelas = $this->db->get('kelas')->result_array();
		$id_sekolah = $this->session->userdata('id_sekolah');
		$this->load->model('presensi_model', 'presensi');
		$data['chart'] = $this->presensi->chart($id_sekolah);
		
		$data['kelas'] = $this->db->get('kelas')->result_array();

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('admin/index', $data);
		$this->load->view('templates/footer');
	}

	public function edit()
	{
		$data['title'] = 'Edit Profile';
		// $data['user'] = $this->db->get_where('user', ['email' =>
		// $this->session->userdata('email')])->row_array();

		
		$data['user'] = $this->db->get_where('sekolah', ['email' =>
		$this->session->userdata('email')])->row_array();

		$this->form_validation->set_rules('nama_depan', 'Nama Depan', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('admin/edit', $data);
			$this->load->view('templates/footer');
		} else {
			$nama_depan = $this->input->post('nama_depan');
			$nama_belakang = $this->input->post('nama_belakang');
			$email = $this->input->post('email');

			$this->db->set('nama_depan', $nama_depan);
			$this->db->set('nama_belakang', $nama_belakang);
			$this->db->where('email', $email);
			$this->db->update('user');

			$this->session->set_flashdata('message', '<div class="alert 
				alert-success" role="alert">Your Profile Has Been Updated</div>');
			redirect('user');
		}
	}

	public function role()
	{
		$data['title'] = 'Role';
		// $data['user'] = $this->db->get_where('user', ['email' =>
		// $this->session->userdata('email')])->row_array();

		
		$data['user'] = $this->db->get_where('sekolah', ['email' =>
		$this->session->userdata('email')])->row_array();
		
		$this->load->model('presensi_model', 'presensi');
		$id_sekolah = $this->session->userdata('id_sekolah');
		
		$data['role'] = $this->presensi->get_Role($id_sekolah);
		// $data['role'] = $this->db->get('user_role')->result_array();

		$this->form_validation->set_rules('role', 'Role', 'required');

		if ($this->form_validation->run() == false) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('admin/role', $data);
			$this->load->view('templates/footer');
		} else {
			$this->db->insert('user_role', ['role' => $this->input->post('role'), 'id_sekolah' => $id_sekolah]);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Role telah ditambahkan!</div');
			redirect('admin/role');
		}
	}

	public function roleAccess($role_id)
	{
		$data['title'] = 'Role Access';
		// $data['user'] = $this->db->get_where('user', ['email' =>
		// $this->session->userdata('email')])->row_array();

		
		$data['user'] = $this->db->get_where('sekolah', ['email' =>
		$this->session->userdata('email')])->row_array();

		$id_sekolah = $this->session->userdata('id_sekolah');
		$data['role'] = $this->db->get_where('user_role', ['id' => $role_id])->row_array();

		$this->db->where('id !=', 1);
		
		$this->db->where('id_sekolah', $id_sekolah);
		$data['menu'] = $this->db->get('user_menu')->result_array();
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('admin/role_access', $data);
		$this->load->view('templates/footer');
	}

	public function changeAccess()
	{
		$menu_id = $this->input->post('menuId');
		$role_id = $this->input->post('roleId');
		$id_sekolah = $this->session->userdata('id_sekolah');

		$data = [
			'role_id' => $role_id,
			'menu_id' => $menu_id,
			'id_sekolah' => $id_sekolah
		];

		$result = $this->db->get_where('user_access_menu', $data);

		if ($result->num_rows() < 1) {
			$this->db->insert('user_access_menu', $data);
		} else {
			$this->db->delete('user_access_menu', $data);
		}

		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Access Changed!</div>');
	}

	public function deleteRole($role_id)
	{
		$this->menu_model->delete_role($role_id);
		// untuk flashdata mempunyai 2 parameter (nama flashdata/alias, isi dari flashdatanya)
		$this->session->set_flashdata('flash-data', 'Role was deleted!');
		$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">The menu has ben deleted!</div>');
		redirect('admin/role', 'refresh');
	}

	public function editRole($id)
	{
		// $data['title'] = 'Form Edit Data Role';
		// $data['user_role'] = $this->menu_model->getRolebyId($role_id);

		// $this->form_validation->set_rules('id', 'ROLE ID', 'required');
		// $this->form_validation->set_rules('role', 'ROLE NAME', 'required');

		// if($this->form_validation->run() == FALSE){
		// 	$this->load->view('templates/header', $data);
		// 	$this->load->view('templates/sidebar', $data);
		// 	$this->load->view('templates/topbar', $data);
		// 	$this->load->view('admin/edit_role', $data);
		// 	$this->load->view('templates/footer');
		// }else{
		// 	$this->menu_model->edit_role($role_id);
		// 	$this->session->set_flashdata('flash-data','Role was edited!');
		// 	redirect('admin/role','refresh');
		// }

		$this->db->update('user_role', ['role' => $this->input->post('role')], ['id' => $id]);
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">The Role has ben edited!</div>');
		redirect('admin/role');
	}

	public function get_presensi()
	{
		$data['title'] = 'Presensi Siswa';
		// $data['user'] = $this->db->get_where('user', ['email' =>
		// $this->session->userdata('email')])->row_array();

		
		$data['user'] = $this->db->get_where('sekolah', ['email' =>
		$this->session->userdata('email')])->row_array();
		$id_sekolah = $this->session->userdata('id_sekolah');
		// var_dump($id_sekolah);
		// die();
		$this->load->model('presensi_model', 'presensi');
		$data['presensi'] = $this->presensi->get_presensi($id_sekolah);
		$data['daftar_user'] = $this->presensi->get_daftar_siswa();
		$data['kelas'] = $this->db->get('kelas')->result_array();
		// $data['daftar_user'] = $this->db->get('user')->result_array();

		$this->form_validation->set_rules('siswa', 'siswa', 'required|trim');
		$this->form_validation->set_rules('status', 'status', 'required|trim');
		$this->form_validation->set_rules('tanggal', 'tanggal', 'required|trim');

		if ($this->form_validation->run() == false) {
			if ($this->session->userdata('role_id') == 1) {
				$this->load->view('templates/header', $data);
				$this->load->view('templates/sidebar');
				$this->load->view('templates/topbar', $data);
				$this->load->view('admin/presensi', $data);
				$this->load->view('templates/footer');
			} else {
				$this->load->view('templates/header', $data);
				$this->load->view('templates/sidebar');
				$this->load->view('templates/topbar', $data);
				$this->load->view('user/presensi', $data);
				$this->load->view('templates/footer');
			}
		} else {
			$siswa = $this->input->post('siswa');
			$tanggal = $this->input->post('tanggal');
			$tanggalHariIni = date('Y-m-d');
			$status = $this->input->post('status');
			$cekAbsen = $this->db->query("SELECT * FROM presensi WHERE id_user = '$siswa' AND tanggal = '$tanggalHariIni' ");
			$data = [
				'tanggal' => $tanggal,
				'status' => $status,
				'id_user' => $siswa,
				'id_sekolah' => $id_sekolah
			];
			// var_dump($data['tanggal']);
			// die();
			if ($cekAbsen->num_rows() >= 1) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Siswa telah Absen Hari Ini !</div');
				redirect('admin/get_presensi', 'refresh');
			}
			$this->db->insert('presensi', $data);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data Absen Telah Ditambahkan!</div');
			redirect('admin/get_presensi', 'refresh');
		}
	}

	public function editPresensi($id)
	{
		$this->db->update('presensi', ['tanggal' => $this->input->post('tanggal')], ['id_presen' => $id]);
		$this->db->update('presensi', ['status' => $this->input->post('status')], ['id_presen' => $id]);
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Presensi has ben edited!</div>');
		redirect('admin/get_presensi', 'refresh');
	}

	public function deletePresensi($presen_id)
	{
		
		$data = $this->db->get_where('presensi', ['id_presen' => $presen_id])->row_array();
		$this->presensi_model->delete_presensi($presen_id);
		unlink('assets/foto/'.$data['image']);
		// $this->db->where('id_presen', $presen_id);
		// $query = $this->db->get('presensi');
		// $row = $query->row();
		// unlink("./assets/foto/$row->image");
		// untuk flashdata mempunyai 2 parameter (nama flashdata/alias, isi dari flashdatanya)
		$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Data Presensi Telah Dihapus!</div');
		redirect('admin/get_presensi', 'refresh');
	}

	public function setting_jam()
	{
		$data['title'] = 'Setting Jam';
		// $data['user'] = $this->db->get_where('user', ['email' =>
		// $this->session->userdata('email')])->row_array();

		
		$data['user'] = $this->db->get_where('sekolah', ['email' =>
		$this->session->userdata('email')])->row_array();
		
		$id_sekolah = $this->session->userdata('id_sekolah');
		$this->db->where('id_sekolah', $id_sekolah);
		$data['setting_jam'] = $this->db->get('jam')->result_array();

		$this->form_validation->set_rules('jamdatang', 'Jam Datang', 'required');
		$this->form_validation->set_rules('jampulang', 'Jam Pulang', 'required');

		if ($this->form_validation->run() == false) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('admin/setting_jam', $data);
			$this->load->view('templates/footer');
		} else {

			$cekJam = $this->db->query("SELECT * FROM jam");
			$jam_datang = $this->input->post('jamdatang');
			$jam_pulang = $this->input->post('jampulang');

			$data = [
				'jam_datang' => $jam_datang,
				'jam_pulang' => $jam_pulang,
				'id_sekolah' => $id_sekolah
			];
			if ($cekJam->num_rows() >= 1) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Hanya dapat 1 kali menambahkan data jam !</div');
				redirect('admin/setting_jam', 'refresh');
			}
			$this->db->insert('jam', $data);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Jam telah ditambahkan!</div');
			redirect('admin/setting_jam');
		}
	}

	public function edit_jam($id)
	{

		$jam_datang = $this->input->post('jamdatang');
		$jam_pulang = $this->input->post('jampulang');
		$id_sekolah = $this->session->userdata('id_sekolah');

		$data = [
			'jam_datang' => $jam_datang,
			'jam_pulang' => $jam_pulang,
			'id_sekolah' => $id_sekolah
		];
		$this->db->update('jam', $data);
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Jam has ben edited!</div>');
		redirect('admin/setting_jam');
	}

	public function deleteJam($jam_id)
	{
		$this->menu_model->delete_jam($jam_id);
		// untuk flashdata mempunyai 2 parameter (nama flashdata/alias, isi dari flashdatanya)
		$this->session->set_flashdata('flash-data', 'Jam was deleted!');
		$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Jam has ben deleted!</div>');
		redirect('admin/setting_jam', 'refresh');
	}

	public function filterTanggal()
	{
		$data['title'] = 'Presensi Siswa';
		// $data['user'] = $this->db->get_where('user', ['email' =>
		// $this->session->userdata('email')])->row_array();

		
		$data['user'] = $this->db->get_where('sekolah', ['email' =>
		$this->session->userdata('email')])->row_array();


		$this->load->model('presensi_model', 'presensi');
		
        $id_sekolah = $this->session->userdata('id_sekolah');
		$data['presensi'] = $this->presensi->get_presensi($id_sekolah);
		$data['daftar_user'] = $this->presensi->get_daftar_siswa();
		$data['kelas'] = $this->db->get('kelas')->result_array();
		// $data['daftar_user'] = $this->db->get('user')->result_array();
		$startdate = $this->input->post('startdate');
		$enddate = $this->input->post('enddate');

		$this->form_validation->set_rules('startdate', 'Start Date', 'required|trim');
		$this->form_validation->set_rules('enddate', 'End Date', 'required|trim');

		if ($this->form_validation->run() == false) {
			if ($this->session->userdata('role_id') == 1) {
				$this->load->view('templates/header', $data);
				$this->load->view('templates/sidebar');
				$this->load->view('templates/topbar', $data);
				$this->load->view('admin/presensi', $data);
				$this->load->view('templates/footer');
			} else {
				$this->load->view('templates/header', $data);
				$this->load->view('templates/sidebar');
				$this->load->view('templates/topbar', $data);
				$this->load->view('user/presensi', $data);
				$this->load->view('templates/footer');
			}
		} else {
			$data['presensi'] = $this->presensi->get_presensi_siswa_pertanggal_admin($startdate, $enddate, $id_sekolah);

			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar');
			$this->load->view('templates/topbar', $data);
			$this->load->view('admin/presensi', $data);
			$this->load->view('templates/footer');
		}
	}

	public function rekapPresensi()
	{
		$data['title'] = 'Rekap Presensi Siswa';
		// $data['user'] = $this->db->get_where('user', ['email' =>
		// $this->session->userdata('email')])->row_array();

		
		$data['user'] = $this->db->get_where('sekolah', ['email' =>
		$this->session->userdata('email')])->row_array();


		$this->load->model('presensi_model', 'presensi');
		$id_sekolah = $this->session->userdata('id_sekolah');
		$data['presensi'] = $this->presensi->get_presensi($id_sekolah);
		// $data['presensi'] = $this->presensi->get_presensi_siswa_lengkap();
		$data['daftar_user'] = $this->presensi->get_daftar_siswa();
		$data['kelas'] = $this->db->get('kelas')->result_array();
		// $data['daftar_user'] = $this->db->get('user')->result_array();

		$this->form_validation->set_rules('siswa', 'siswa', 'required|trim');
		$this->form_validation->set_rules('status', 'status', 'required|trim');
		$this->form_validation->set_rules('tanggal', 'tanggal', 'required|trim');

		if ($this->form_validation->run() == false) {
			if ($this->session->userdata('role_id') == 1) {
				$this->load->view('templates/header', $data);
				$this->load->view('templates/sidebar');
				$this->load->view('templates/topbar', $data);
				$this->load->view('admin/rekap_presensi', $data);
				$this->load->view('templates/footer');
			} else {
				$this->load->view('templates/header', $data);
				$this->load->view('templates/sidebar');
				$this->load->view('templates/topbar', $data);
				$this->load->view('user/presensi', $data);
				$this->load->view('templates/footer');
			}
		} else {
			$siswa = $this->input->post('siswa');
			$tanggal = $this->input->post('tanggal');
			$tanggalHariIni = date('Y-m-d');
			$status = $this->input->post('status');
			$cekAbsen = $this->db->query("SELECT * FROM presensi WHERE id_user = '$siswa' AND tanggal = '$tanggalHariIni' ");
			$data = [
				'tanggal' => $tanggal,
				'status' => $status,
				'id_user' => $siswa
			];
			// var_dump($data['tanggal']);
			// die();
			if ($cekAbsen->num_rows() >= 1) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Siswa telah Absen Hari Ini !</div');
				redirect('admin/get_presensi', 'refresh');
			}
			$this->db->insert('presensi', $data);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data Absen Telah Ditambahkan!</div');
			redirect('admin/get_presensi', 'refresh');
		}
	}

	public function filterKelas()
	{
		$data['title'] = 'Rekap Presensi Siswa';
		// $data['user'] = $this->db->get_where('user', ['email' =>
		// $this->session->userdata('email')])->row_array();

		
		$data['user'] = $this->db->get_where('sekolah', ['email' =>
		$this->session->userdata('email')])->row_array();


		$this->load->model('presensi_model', 'presensi');
		
        $id_sekolah = $this->session->userdata('id_sekolah');
		$data['presensi'] = $this->presensi->get_presensi($id_sekolah);
		$data['daftar_user'] = $this->presensi->get_daftar_siswa();
		$data['kelas'] = $this->db->get('kelas')->result_array();
		// $data['daftar_user'] = $this->db->get('user')->result_array();
		$kelas = $this->input->post('kelas');
		$startdate = $this->input->post('startdate');
		$enddate = $this->input->post('enddate');
		$data['startdate'] = $this->input->post('startdate');
		$data['enddate'] = $this->input->post('enddate');

		$this->form_validation->set_rules('kelas', 'Kelas', 'required|trim');
		$this->form_validation->set_rules('startdate', 'Start Date', 'required|trim');
		$this->form_validation->set_rules('enddate', 'End Date', 'required|trim');

		if ($this->form_validation->run() == false) {
			if ($this->session->userdata('role_id') == 1) {
				$this->load->view('templates/header', $data);
				$this->load->view('templates/sidebar');
				$this->load->view('templates/topbar', $data);
				$this->load->view('admin/rekap_presensi', $data);
				$this->load->view('templates/footer');
			} else {
				$this->load->view('templates/header', $data);
				$this->load->view('templates/sidebar');
				$this->load->view('templates/topbar', $data);
				$this->load->view('user/presensi', $data);
				$this->load->view('templates/footer');
			}
		} else {
			$data['presensi'] = $this->presensi->get_presensi_siswa_lengkap($kelas, $startdate, $enddate, $id_sekolah);
			$data['startdate'] = $this->input->post('startdate');
			$data['enddate'] = $this->input->post('enddate');

			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar');
			$this->load->view('templates/topbar', $data);
			$this->load->view('admin/rekap_presensi', $data);
			$this->load->view('templates/footer');
		}
	}

	function tgl_indo($tanggal){
		$bulan = array (
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('-', $tanggal);
		
		// variabel pecahkan 0 = tanggal
		// variabel pecahkan 1 = bulan
		// variabel pecahkan 2 = tahun
	 
		return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
	}

	public function create()
	{
		$data['title'] = "Upload File Excel";
		// $data['user'] = $this->db->get_where('user', ['email' =>
		// $this->session->userdata('email')])->row_array();

		
		$data['user'] = $this->db->get_where('sekolah', ['email' =>
		$this->session->userdata('email')])->row_array();

		$this->load->model('presensi_model', 'presensi');
		$enddate = $this->input->post('enddate');
		$kelas = $this->input->post('kelas');
		$startdate = $this->input->post('startdate');
		$data['presensi'] = $this->presensi->get_presensi_siswa_lengkap($kelas, $startdate, $enddate);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar');
		$this->load->view('templates/topbar', $data);
		$this->load->view('import/create', $data);
		$this->load->view('templates/footer');
	}
	
	public function siswa(){
		$data['title'] = 'Data Siswa';
		// $data['user'] = $this->db->get_where('user', ['email' =>
		// $this->session->userdata('email')])->row_array();

		
		$data['user'] = $this->db->get_where('sekolah', ['email' =>
		$this->session->userdata('email')])->row_array();
		
		
		$id_sekolah = $this->session->userdata('id_sekolah');
		$this->db->where('id_sekolah', $id_sekolah);
		$data['siswa'] = $this->db->get('user')->result_array();
		$data['kelas'] = $this->db->get('kelas')->result_array();
		// $data['siswa'] = $this->db->get('user')->result_array();

		$this->form_validation->set_rules('nama_depan', 'Nama Depan', 'required|trim');
		$this->form_validation->set_rules('nama_belakang', 'Nama Belakang', 'required|trim');
		$this->form_validation->set_rules('kelas', 'Kelas', 'required|trim');
		$this->form_validation->set_rules('no_induk', 'No Induk', 'required|trim|is_unique[user.no_induk]', ['is_unique' => 'This No Induk has already registered!']);
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', ['is_unique' => 'This email has already registered!']);
		$this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[5]|matches[password2]', ['matches' => 'password dont match!', 'min_length' => 'password too short!']);
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

		if ($this->form_validation->run() == false) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('admin/siswa', $data);
			$this->load->view('templates/footer');
		} else {
			$email = $this->input->post('email');
			$data = [
				'nama_depan'		=> $this->input->post('nama_depan'),
				'nama_belakang'		=> $this->input->post('nama_belakang'),
				'no_induk'		=> $this->input->post('no_induk'),
				'email'		=> htmlspecialchars($email),
				'kelas'		=> $this->input->post('kelas'),
				'password'		=> password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
				'role_id'	=> 2,
				'is_active' =>1,
				'date_created'	=> date('Y-m-d h:i:sa'),
				'id_sekolah'	=> $id_sekolah

			];
			// var_dump($data);
			// die();
			$this->db->insert('user', $data);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Siswa telah ditambahkan!</div');
			redirect('admin/role');
		}
	}

	public function editSiswa($id){
		$this->db->update('user', ['nama_depan' => $this->input->post('nama_depan')], ['id' => $id]);
		$this->db->update('user', ['nama_belakang' => $this->input->post('nama_belakang')], ['id' => $id]);
		$this->db->update('user', ['email' => $this->input->post('email')], ['id' => $id]);
		$this->db->update('user', ['no_induk' => $this->input->post('no_induk')], ['id' => $id]);
		$this->db->update('user', ['kelas' => $this->input->post('kelas')], ['id' => $id]);
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Siswa has ben edited!</div>');
		redirect('admin/siswa', 'refresh');
	}

	public function deleteSiswa($id){
		$this->presensi_model->delete_presensi($id);
		$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Data Siswa Telah Dihapus!</div');
		redirect('admin/siswa', 'refresh');
	}
}
