<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require('./application/third_party/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Export extends CI_Controller {
	 
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('presensi_model', 'presensi');
	}

	public function index()
	{
		
		$data['presensi'] = $this->presensi->get_presensi();
		$this->load->view('admin/rekap_presensi');
	}

	public function export()
	{
		$data['title'] = 'Rekap Presensi Siswa';
		$data['user'] = $this->db->get_where('user', ['email' =>
		$this->session->userdata('email')])->row_array();


		$this->load->model('presensi_model', 'presensi');
		
        $id_sekolah = $this->session->userdata('id_sekolah');
		$data['presensi'] = $this->presensi->get_presensi($id_sekolah);
		$data['daftar_user'] = $this->presensi->get_daftar_siswa();
		$data['kelas'] = $this->db->get('kelas')->result_array();
		// $dataPresensi = $this->presensi->get_presensi();
		$kelas = $this->input->post('kelas2');
		$startdate = $this->input->post('startdate'); 
		$enddate = $this->input->post('enddate'); 
		$dataPresensi2 = $this->presensi->get_presensi_siswa_lengkap($kelas, $startdate, $enddate, $id_sekolah);

		$spreadsheet = new Spreadsheet;

		$this->form_validation->set_rules('kelas2', 'Kelas', 'required|trim');
		$this->form_validation->set_rules('startdate', 'Start Date', 'required|trim');
		$this->form_validation->set_rules('enddate', 'End Date', 'required|trim');

		if ($this->form_validation->run() == false) {
			if ($this->session->userdata('role_id') == 1) {
				$this->load->view('templates/header', $data);
				$this->load->view('templates/sidebar');
				$this->load->view('templates/topbar', $data);
				$this->load->view('admin/rekap_presensi', $data);
				$this->load->view('templates/footer');
			} else {
				$this->load->view('templates/header', $data);
				$this->load->view('templates/sidebar');
				$this->load->view('templates/topbar', $data);
				$this->load->view('user/presensi', $data);
				$this->load->view('templates/footer');
			}
		} else {
			$spreadsheet->setActiveSheetIndex(0)
					->setCellValue('A1', 'No')
					->setCellValue('B1', 'No Induk')
					->setCellValue('C1', 'Nama')	
					->setCellValue('D1', 'Kelas')	
					->setCellValue('E1', 'Hadir')	
					->setCellValue('F1', 'Terlambat')	
					->setCellValue('G1', 'Izin')
					->setCellValue('H1', 'Sakit')
					->setCellValue('I1', 'Alpha')
					->setCellValue('J1', 'Total');
		
					$kolom = 2;
					$nomor = 1;
					foreach($dataPresensi2 as $d) {
		  
						 $spreadsheet->setActiveSheetIndex(0)
									 ->setCellValue('A' . $kolom, $nomor)
									 ->setCellValue('B' . $kolom, $d['no_induk'])
									 ->setCellValue('C' . $kolom, $d['nama_depan'] . " " . $d['nama_belakang'])
									 ->setCellValue('D' . $kolom, $d['kelas'])
									//  ->setCellValue('D' . $kolom, date('d-m-Y', strtotime($d['tanggal'])))
									 ->setCellValue('E' . $kolom, $d['hadir'])
									 ->setCellValue('F' . $kolom, $d['terlambat'])
									 ->setCellValue('G' . $kolom, $d['izin'])
									 ->setCellValue('H' . $kolom, $d['sakit'])
									 ->setCellValue('I' . $kolom, $d['alpha'])
									 ->setCellValue('J' . $kolom, $d['total']);
		  
						 $kolom++;
						 $nomor++;
		  
					}
		  
					$writer = new Xlsx($spreadsheet);
					
					$filename = 'Laporan Absensi' . '_Kelas_'. $kelas. '_' . $this->tgl_indo(date('Y-m-d', strtotime($startdate))) . '-' . $this->tgl_indo(date('Y-m-d', strtotime($enddate))) .'.xlsx'; 

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="'. $filename .'"');
				header('Cache-Control: max-age=0');
		  
				$writer->save('php://output');
		}
			   }

			   function tgl_indo($tanggal){
				$bulan = array (
					1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
				$pecahkan = explode('-', $tanggal);
				
				// variabel pecahkan 0 = tanggal
				// variabel pecahkan 1 = bulan
				// variabel pecahkan 2 = tahun
			 
				return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
			}
	}
