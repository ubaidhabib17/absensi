<?php
defined('BASEPATH') or exit('No direct script access');

class Presensi_model extends CI_MODEL {

    public function __construct()
	{
		parent::__construct();
	}
	
	public function getRolebyId($id){
        return $this->db->get_where('presensi', ['id_presen' => $id])->row_array();
    }
    
    function get_presensi($id_sekolah)
	{
		$query = "SELECT `presensi`.*, `user`.*
        FROM `user` JOIN `presensi`
        ON `user`.`id` = `presensi`.`id_user`
		WHERE `presensi`.`id_sekolah` = $id_sekolah;
        ";
		return $this->db->query($query)->result_array();
	}
	
	function get_Role($id_sekolah)
	{
		$query = "SELECT * FROM user_role  
        WHERE user_role.id_sekolah = $id_sekolah";
        
        return $this->db->query($query)->result_array();
    }
    
    public function delete_presensi($presensi_id){
		$this->db->where('id_presen', $presensi_id);
		$this->db->delete('presensi');
		
	}
	
	public function delete_siswa($id){
		$this->db->where('id', $id);
		$this->db->delete('user');
	}

    public function edit_presensi($id_presen){
        $data=[
            "id_presen" => $this->input->post('id_presen', true),
            "status" => $this->input->post('status', true)
        ];

        $this->db->where('id_presen', $this->input->post('id_presen'));
        $this->db->update('presensi', $data);
    }
    
    function get_presensi_siswa($id, $id_sekolah){
        $query = "SELECT * FROM USER JOIN presensi 
        on USER.id = presensi.id_user 
        WHERE presensi.id_user = $id AND presensi.id_sekolah = $id_sekolah";
        
        return $this->db->query($query)->result_array();
        
	}

	function get_daftar_siswa(){
		$query = "SELECT * FROM USER
		WHERE role_id = 2;
        ";
		return $this->db->query($query)->result_array();
	}

	function get_jam(){
		$query = "SELECT jam_datang, jam_pulang FROM jam";
		return $this->db->query($query)->row();
	}

	function cek_absen($id_user, $tanggalHariIni){
		$query = "SELECT * FROM presensi
		WHERE id_user = '$id_user' AND tanggal = '$tanggalHariIni' ";

		return $this->db->query($query)->result_array();
	}

	function get_presensi_siswa_pertanggal($id, $startdate, $enddate, $id_sekolah){
		$query = "SELECT * FROM USER JOIN presensi 
        on USER.id = presensi.id_user 
		WHERE presensi.id_user = $id AND presensi.id_sekolah = $id_sekolah AND presensi.tanggal BETWEEN '$startdate' AND '$enddate'
		ORDER BY tanggal";
        
        return $this->db->query($query)->result_array();
	}

	function get_presensi_siswa_pertanggal_admin($startdate, $enddate, $id_sekolah){
		// $query = "SELECT * FROM USER JOIN presensi 
        // on USER.id = presensi.id_user 
		// WHERE presensi.tanggal BETWEEN '$startdate' AND '$enddate'
		// ORDER BY tanggal";

		$query = "SELECT `presensi`.*, `user`.*
		FROM `user` JOIN `presensi`
		ON `user`.`id` = `presensi`.`id_user`
		WHERE `presensi`.`id_sekolah` = $id_sekolah AND `presensi`.`tanggal` BETWEEN '$startdate' AND '$enddate'";
        
        return $this->db->query($query)->result_array();
	}

	function get_presensi_siswa_perkelas($kelas, $startdate, $enddate){
		$query = "SELECT `presensi`.*, `user`.*
		FROM `user` JOIN `presensi`
		ON `user`.`id` = `presensi`.`id_user`
		WHERE `user`.`kelas` = '$kelas' AND `presensi`.`tanggal` BETWEEN '$startdate' AND '$enddate'";

		// $query = "SELECT DISTINCT id_user,
		// (SELECT COUNT(status)
		// FROM presensi
		// WHERE (status = 'Hadir')) AS Hadir,
		// (SELECT COUNT(status)
		// FROM presensi
		// WHERE(status = 'Terlambat')) AS Terlambat,
		// (SELECT COUNT(status)
		// FROM presensi
		// WHERE(status = 'Alpha')) AS Alpha,
		// (SELECT COUNT(status)
		// FROM presensi
		// WHERE (status = 'Hadir') OR (status = 'Terlambat') OR (status = 'Alpha')) AS Total
		// FROM presensi";
        
        return $this->db->query($query)->result_array();
	}

	function get_presensi_siswa_lengkap($kelas, $startdate, $enddate, $id_sekolah){
		// $query = "SELECT `presensi`.*, `user`.*
		// FROM `user` JOIN `presensi`
		// ON `user`.`id` = `presensi`.`id_user`
		// WHERE `user`.`kelas` = '$kelas' AND `presensi`.`tanggal` BETWEEN '$startdate' AND '$enddate',
		// (SELECT COUNT(`presensi`.`status`)
		// FROM presensi
		// WHERE (status = 'Hadir')) AS Hadir,
		// (SELECT COUNT(status)
		// FROM presensi
		// WHERE(status = 'Terlambat')) AS Terlambat,
		// (SELECT COUNT(status)
		// FROM presensi
		// WHERE(status = 'Alpha')) AS Alpha,
		// (SELECT COUNT(status)
		// FROM presensi
		// WHERE(status = 'Izin')) AS Izin,
		// (SELECT COUNT(status)
		// FROM presensi
		// WHERE(status = 'Sakit')) AS Sakit,
		// (SELECT COUNT(status)
		// FROM presensi
		// WHERE (status = 'Hadir') OR (status = 'Terlambat') OR (status = 'Alpha') OR (status = 'Izin') OR (status = 'Sakit') ) AS Total
		// FROM presensi
		// GROUP BY id_user";

		// $query = "SELECT NAMA_KARYAWAN,
		// 	sum(case when Absensi=’S’ then 1 else 0 end) as S,
		// 	sum(case when Absensi=’I’ then 1 else 0 end) as I,
		// 	sum(case when Absensi=’A’ then 1 else 0 end) as A,
	  	// 	sum(case when Absensi=’S’ then 1 when Absensi=’I’ then 1 when Absensi=’A’ then 1 else 0 end) as Total
		// FROM presensi
		// GROUP BY id_user";

		$query = "SELECT `presensi`.*, `user`.*,
			COUNT(IF(status = 'Hadir', status, NULL)) AS hadir,
        	COUNT(IF(status = 'Terlambat', status, NULL)) AS terlambat,
        	COUNT(IF(status = 'Izin', status, NULL)) AS izin,       
        	COUNT(IF(status = 'Sakit', status, NULL)) AS sakit,       
        	COUNT(IF(status = 'Alpha', status, NULL)) AS alpha,
			SUM(case when status ='Hadir' then 1 when status ='Terlambat' then 1 when status ='Alpha' then 1 when status ='Sakit' then 1 when status ='Izin' then 1 else 0 end) as total       
		FROM `user` JOIN `presensi`
		ON `user`.`id` = `presensi`.`id_user`
		WHERE `user`.`kelas` = '$kelas' AND `presensi`.`id_sekolah` = $id_sekolah AND  `presensi`.`tanggal` BETWEEN '$startdate' AND '$enddate'";

		// $query = "SELECT COUNT(*) FROM presensi WHERE id_user = '118' AND status ='H'";

		return $this->db->query($query)->result_array();
	}

	function chart($id_sekolah){
		$query = "SELECT `presensi`.*, `user`.*,
			COUNT(IF(status = 'Hadir', status, NULL)) AS hadir,
        	COUNT(IF(status = 'Terlambat', status, NULL)) AS terlambat,
        	COUNT(IF(status = 'Izin', status, NULL)) AS izin,       
        	COUNT(IF(status = 'Sakit', status, NULL)) AS sakit,       
        	COUNT(IF(status = 'Alpha', status, NULL)) AS alpha,
			SUM(case when status ='Hadir' then 1 when status ='Terlambat' then 1 when status ='Alpha' then 1 when status ='Sakit' then 1 when status ='Izin' then 1 else 0 end) as total       
		FROM `user` JOIN `presensi`
		ON `user`.`id` = `presensi`.`id_user`
		WHERE `presensi`.`id_sekolah` = '$id_sekolah'";

		// $query = "SELECT COUNT(*) FROM presensi WHERE id_user = '118' AND status ='H'";

		return $this->db->query($query)->result_array();
	} 
}
