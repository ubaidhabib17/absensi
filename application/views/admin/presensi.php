		<!-- Begin Page Content -->
		<style>
			.dataTables_filter {
				display: none;
			}
			#myImg:hover {
				opacity: 0.7;
			}
		</style>
		<div class="container-fluid">

			<!-- Page Heading -->
			<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
			<span id="span" class="display-1 text-dark d-flex justify-content-center"></span>

			<?= $this->session->flashdata('message'); ?>
			<div class="col-lg">
				<a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newMenuModal">Add presensi</a>
				<?= form_error('presensi', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
				<?php if (validation_errors()) : ?>
					<div class="alert alert-danger" role="alert">
						<?= validation_errors(); ?>
					</div>
				<?php endif; ?>
				<form action="<?= base_url('admin/filterTanggal') ?>" method="POST" id="cari">
					<div class="form-group row">
						<div class="col-sm-2">
							<label for="stardate">Mulai Tanggal</label>
							<!-- <?= form_error('startdate', '<small class="text-danger pl-3">', '</small>'); ?> -->
							<input type="date" class="form-control" name="startdate" placeholder="Start Date">
						</div>
						<div class="col-sm-2">
							<label for="enddate">Sampai Tanggal</label>
							<!-- <?= form_error('enddate', '<small class="text-danger pl-3">', '</small>'); ?> -->
							<input type="date" class="form-control" name="enddate" placeholder="End Date">
						</div>
						<div class="col-sm-3">
							<br><button type="submit" class="btn btn-primary mt-2" id="cari" name="cari">Cari</button>
						</div>
					</div>
				</form>
				<button class="btn btn-warning mb-2" id="refresh1">Refresh Page</button><br><br>
				<div class="box-body table-responsive">
					<!-- <button id="Reset">Reset</button> -->
					<table class="table table-bordered table-striped" id="dataTabless" width="100%">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col" id="no_induk">No induk </th>
								<th scope="col" id="nama">Nama </th>
								<th scope="col" id="kelas" style="width: 10px;">Kelas </th>
								<th scope="col" id="tanggal" style="width: 120px;">Tanggal</th>
								<th scope="col" id="status">Status</th>
								<th scope="col" id="lokasi">Lokasi</th>
								<th scope="col" id="Foto">Foto</th>
								<th scope="col" style="width: 170px;">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; ?>
							<?php foreach ($presensi as $p) : ?>
								<tr>
									<th scope="row"><?= $i; ?></th>
									<td><?= $p['no_induk']; ?></td>
									<td><?= $p['nama_depan'] . " " . $p['nama_belakang']; ?></td>
									<td><?= $p['kelas']; ?></td>
									<td><?= date('d-m-Y', strtotime($p['tanggal'])) ?></td>
									<td><?= $p['status']; ?></td>
									<td><a target="_blank" href="https://www.google.com/maps/search/?api=1&query=<?= $p['latitude']; ?>,<?= $p['longitude'] ?>">Lihat Lokasi</a></td>
									<td>
										<a href="" id="pop" class="pup" data-toggle="modal" data-target="#exampleModal<?= $p['id_presen'] ?>">
											<img src="<?= base_url(); ?>assets/foto/<?= $p['image'] ?>" alt="" id="myImg" data-toggle="modal" data-target="#exampleModal<?= $p['id_presen'] ?>" style="width: 100%;">
										</a>
									</td>
									<td>
										<a href="" data-toggle="modal" data-target="#editPresensiModal<?= $p['id_presen'] ?>" class="badge badge-success">edit</a>
										<a href="<?= base_url('admin/deletePresensi/') . $p['id_presen']; ?>" class="badge badge-danger" onclick="return confirm('Are you sure to delete this data ?');">delete</a>
									</td>
								</tr>
								<?php $i++; ?>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>

			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- End of Main Content -->

		<!-- Modal -->
		<div class="modal fade" id="newMenuModal" tabindex="-1" aria-labelledby="newMenuModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="newMenuModalLabel">Add Presensi</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form action="<?= base_url('admin/get_presensi'); ?>" method="POST">
						<div class="modal-body">
							<div class="form-group">
								<select class="form-control js-example-basic-single siswa" style="width:100%" id="siswa" name="siswa">
									<!-- <option value="" disabled>Pilih Kehadiran</option> -->
									<option value=""></option>
									<?php foreach ($daftar_user as $du) { ?>
										<option name="siswa" value="<?= $du['id']; ?>">
											<?= $du['nama_depan'] . " " . $du['nama_belakang']; ?></option>
									<?php
									} ?>
								</select>
								<?= form_error('status', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
							<div class="form-group">
								<input type="date" class="form-control" value="" id="tanggal" data-date-format="DD/MMM/YYYY" name="tanggal" placeholder="tanggal">
								<?= form_error('tanggal', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
							<div class="form-group">
								<label for="status">Status </label>
								<select class="form-control js-example-basic-single" style="width:100%" id="status" name="status">
									<option value=""></option>
									<option value="Izin">Izin</option>
									<option value="Alpha">Alpha</option>
									<option value="Sakit">Sakit</option>
								</select>
								<?= form_error('status', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="Submit" class="btn btn-primary">Add</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- Edit Modal -->
		<?php foreach ($presensi as $p) : ?>
			<div class="modal fade" id="editPresensiModal<?= $p['id_presen'] ?>" tabindex="-1" role="dialog" aria-labelledby="editPresensiModal<?= $p['id_presen'] ?>Label" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="editMenuModal<?= $p['id_presen'] ?>">Edit Presensi</h5>
							<buttond type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</buttond>
						</div>
						<form action="<?= base_url('admin/editPresensi/' . $p['id_presen']); ?>" method="post">
							<div class="modal-body">
								<div class="form-group">
									<input type="text" class="form-control" readonly value="<?= $p['nama_depan'] . " " . $p['nama_belakang']; ?>" id="nama" name="nama" placeholder="Submenu title">
								</div>
								<div class="form-group">
									<input type="date" class="form-control" value="<?= date('Y-m-d', strtotime($p['tanggal'])); ?>" id="tanggal" name="tanggal" placeholder="Submenu url">
								</div>
								<div class="form-group">
									<select class="form-control js-example-basic-single status2" id="status2" name="status" style="width:100%">
										<option value="<?= $p['status'] ?>"><?= $p['status']; ?></option>
										<option value="Hadir">Hadir</option>
										<option value="Izin">Izin</option>
										<option value="Alpha">Alpha</option>
									</select>
									<br><img src="<?= base_url(); ?>assets/foto/<?= $p['image'] ?>" id="imagepreview" style="width: 100%; height: 100%;">
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
								<button type="submit" class="btn btn-primary">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			<!-- Modal  Foto-->
			<div class="modal fade" id="exampleModal<?= $p['id_presen'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Foto Siswa</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<label for="" class="h5">Nama = <?= $p['nama_depan'] . " " . $p['nama_belakang']; ?></label><br>
							<label for="" class="h5">Kelas = <?= $p['kelas']; ?></label>
							<img src="<?= base_url(); ?>assets/foto/<?= $p['image'] ?>" id="imagepreview" style="width: 100%; height: 100%;">
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
		<!-- End Edit Modal -->
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
		<script language="JavaScript">
			$(document).ready(function() {
				$("#refresh1").click(function () {
					window.location.href = window.location.href
				});
				var table1 = $('#dataTabless').DataTable({
					responsive: true
				});
				$('.js-example-basic-single').select2({
					placeholder: "Pilih Status",
					allowClear: true
				});
				$('#siswa').select2({
					placeholder: "Pilih Siswa",
					allowClear: true
				});
				$('.filter').on('change', function() {
					let siswa = $("#filter-siswa").val()
					let kelas = $("#filter-kelas").val()
					console.log([siswa, kelas])
				});
				$('#status2').select2({});

				$("#tanggal").on("change", function() {
					this.setAttribute(
						"data-date",
						moment(this.value, "YYYY-MM-DD")
						.format(this.getAttribute("data-date-format"))
					)
				}).trigger("change");

				$('#tanggal').change(function() {
					alert(this.value.split("-").reverse().join("-"));
				});

				var span = document.getElementById('span');

				function time() {
					var d = new Date();
					var s = d.getSeconds();
					var m = d.getMinutes();
					var h = d.getHours();
					span.textContent = h + ":" + m + ":" + s;
				}

				setInterval(time, 1000);

				$('#no_induk').each(function() {
					var title = $(this).text();
					$(this).html('<input type="text" placeholder="Cari ' + title + '" style="width: 110px;" />');
				});
				$('#nama').each(function() {
					var title = $(this).text();
					$(this).html('<select class="form-control js-example-basic-single siswa" style="width:125px;"  id="selectt" name="siswa"><option value=""></option><?php foreach ($daftar_user as $du) { ?><option name="siswa" value="<?= $du['nama_depan'] . " " . $du['nama_belakang']; ?>"><?= $du['nama_depan'] . " " . $du['nama_belakang']; ?></option><?php } ?></select>');
				});
				$('#selectt').select2({
					placeholder: "Pilih Siswa",
					allowClear: true
				});
				$('#kelas').each(function() {
					var title = $(this).text();
					$(this).html('<select class="form-control js-example-basic-single kelas" style="width:107px;"  id="selectkelas" name="kelas"><option value=""></option><?php foreach ($kelas as $k) { ?><option name="kelas" value="<?= $k['kelas']; ?>"><?= $k['kelas']; ?></option><?php } ?></select>');
				});
				$('#selectkelas').select2({
					placeholder: "Pilih Kelas",
					allowClear: true
				});
				// $('#tanggal').each(function () {
				// 	var title = $(this).text();
				// 	$(this).html('<input type="date" dateformat="d M y" class="clearable" placeholder="Cari ' + title +
				// 		'" style="width: 120px;"/>');
				// });
				$('#status').each(function() {
					var title = $(this).text();
					$(this).html('<input type="text" placeholder="Cari ' + title + '" style="width: 86px;"/>');
				});

				table1.columns().every(function() {
					var that = this;

					$('select', this.header()).on('change', function() {
						if (that.search() !== this.value) {
							that
								.search(this.value)
								.draw();
						}
					});

					$('input', this.header()).on('keyup change', function() {
						if (that.search() !== this.value) {
							that
								.search(this.value)
								.draw();
						}
					});
				});
			});
		</script>
