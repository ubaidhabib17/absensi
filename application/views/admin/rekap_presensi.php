<!-- Begin Page Content -->
<style>
	.dataTables_filter {
		display: none;
	}
</style>
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
	<span id="span" class="display-1 text-dark d-flex justify-content-center"></span>

	<?= $this->session->flashdata('message'); ?>
	<div class="col-lg">
		<!-- <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newMenuModal">Add presensi</a> -->
		<?= form_error('presensi', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
		<?php if (validation_errors()) : ?>
			<div class="alert alert-danger" role="alert">
				<?= validation_errors(); ?>
			</div>
		<?php endif; ?>

		<form action="<?= base_url('admin/filterKelas') ?>" method="POST" id="cari">
			<div class="form-group row">
				<div class="col-sm-2">
					<label for="stardate">Mulai Tanggal</label>
					<!-- <?= form_error('startdate', '<small class="text-danger pl-3">', '</small>'); ?> -->
					<input type="date" class="form-control" name="startdate" placeholder="Start Date">
				</div>
				<div class="col-sm-2">
					<label for="enddate">Sampai Tanggal</label>
					<!-- <?= form_error('enddate', '<small class="text-danger pl-3">', '</small>'); ?> -->
					<input type="date" class="form-control" name="enddate" placeholder="End Date">
				</div>
				<div class="col-sm-2">
					<label for="kelas">Kelas</label>
					<!-- <?= form_error('kelas', '<small class="text-danger pl-3">', '</small>'); ?> -->
					<select class="form-control js-example-basic-single kelas" style="width:100%" id="kelas" name="kelas">
						<!-- <option value="" disabled>Pilih Kehadiran</option> -->
						<option value=""></option>
						<?php foreach ($kelas as $k) { ?>
							<option name="kelas" value="<?= $k['kelas']; ?>">
								<?= $k['kelas']; ?></option>
						<?php
						} ?>
					</select>
				</div>
				<div class="col-sm-3">
					<br><button type="submit" class="btn btn-outline-primary mt-2 mb-1 ml-2" id="cari" name="cari"><span class="fas fa-search mr-1"></span>Cari</button>
				</div>
			</div>
		</form>

		<?php if (isset($startdate)) { ?>
			<p>Rekap Presensi Siswa Mulai Tanggal <?= date('d F Y', strtotime($startdate)); ?> Sampai Tanggal <?= date('d F Y', strtotime($enddate)); ?></p>
		<?php } ?>

		<form action="<?= base_url('Export/export') ?>" method="POST">
		<div class="form-group row">
				<div class="col-sm-2">
					<label for="stardate">Mulai Tanggal</label>
					<!-- <?= form_error('startdate', '<small class="text-danger pl-3">', '</small>'); ?> -->
					<input type="date" class="form-control" name="startdate" placeholder="Start Date">
				</div>
				<div class="col-sm-2">
					<label for="enddate">Sampai Tanggal</label>
					<!-- <?= form_error('enddate', '<small class="text-danger pl-3">', '</small>'); ?> -->
					<input type="date" class="form-control" name="enddate" placeholder="End Date">
				</div>
				<div class="col-sm-2">
					<label for="kelas">Kelas</label>
					<!-- <?= form_error('kelas', '<small class="text-danger pl-3">', '</small>'); ?> -->
					<select class="form-control js-example-basic-single kelas" style="width:100%" id="kelas2" name="kelas2">
						<!-- <option value="" disabled>Pilih Kehadiran</option> -->
						<option value=""></option>
						<?php foreach ($kelas as $k) { ?>
							<option name="kelas" value="<?= $k['kelas']; ?>">
								<?= $k['kelas']; ?></option>
						<?php
						} ?>
					</select>
				</div>
				<div class="col-sm-3">
					<br><button type="submit" class="btn btn-outline-primary mt-2 mb-1 ml-2"><span class="fas fa-file-download mr-1"></span>Cetak</button>
				</div>
			</div>
		</form>
		<div class="form-group row">
			<button class="btn btn-outline-warning mb-2 ml-3" id="refresh"><span class="fas fa-redo mr-1"></span>Refresh Page</button>
			<!-- <a class="btn btn-outline-primary mb-2 ml-3" href="<?= base_url('Export/export'); ?>"><span class="fa fa-download mr-1"></span>Export Absensi</a> -->
			<!-- <a class="btn btn-outline-success mb-2 ml-3" href="<?= base_url('admin/create'); ?>"><span class="fas fa-file-excel mr-1"></span>Import Absensi</a> -->
		</div>
		<div class="box-body table-responsive">
			<!-- <button id="Reset">Reset</button> -->
			<table class="table table-bordered table-striped" id="dataTabless" width="100%">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col" id="no_induk">No induk </th>
						<th scope="col" id="nama">Nama </th>
						<th scope="col" id="kelas">Kelas </th>
						<th scope="col" id="status">Hadir</th>
						<th scope="col" id="lokasi">Terlambat</th>
						<th scope="col">Izin</th>
						<th scope="col">Sakit</th>
						<th scope="col">Alpha</th>
						<th scope="col">Total</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; ?>
					<?php foreach ($presensi as $p) : ?>
						<?php
						if (!isset($p['hadir'])) {
							$p['hadir'] = "0";
						}
						if (!isset($p['terlambat'])) {
							$p['terlambat'] = "0";
						}
						if (!isset($p['izin'])) {
							$p['izin'] = "0";
						}
						if (!isset($p['sakit'])) {
							$p['sakit'] = "0";
						}
						if (!isset($p['alpha'])) {
							$p['alpha'] = "0";
						}
						if (!isset($p['total'])) {
							$p['total'] = "0";
						}

						?>
						<tr>
							<th scope="row"><?= $i; ?></th>
							<td><?= $p['no_induk']; ?></td>
							<td><?= $p['nama_depan'] . " " . $p['nama_belakang']; ?></td>
							<td><?= $p['kelas']; ?></td>
							<td><?= $p['hadir']; ?></td>
							<td><?= $p['terlambat'] ?></td>
							<td><?= $p['izin'] ?></td>
							<td><?= $p['sakit'] ?></td>
							<td><?= $p['alpha'] ?></td>
							<td><?= $p['total'] ?></td>
							<!-- <td><?= $p['status']; ?></td>
								<td><a target="_blank" href="https://www.google.com/maps/search/?api=1&query=<?= $p['latitude']; ?>,<?= $p['longitude'] ?>">Lihat Lokasi</a></td>
								<td>
									<a href="" data-toggle="modal" data-target="#editPresensiModal<?= $p['id_presen'] ?>"
										class="badge badge-success">edit</a>
									<a href="<?= base_url('admin/deletePresensi/') . $p['id_presen']; ?>"
										class="badge badge-danger"
										onclick="return confirm('Are you sure to delete this data ?');">delete</a>
								</td> -->
						</tr>
						<?php $i++; ?>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>

	</div>
	<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Modal -->
<div class="modal fade" id="newMenuModal" tabindex="-1" aria-labelledby="newMenuModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="newMenuModalLabel">Add Presensi</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="<?= base_url('admin/get_presensi'); ?>" method="POST">
				<div class="modal-body">
					<div class="form-group">
						<select class="form-control js-example-basic-single siswa" style="width:100%" id="siswa" name="siswa">
							<!-- <option value="" disabled>Pilih Kehadiran</option> -->
							<option value=""></option>
							<?php foreach ($daftar_user as $du) { ?>
								<option name="siswa" value="<?= $du['id']; ?>">
									<?= $du['nama_depan'] . " " . $du['nama_belakang']; ?></option>
							<?php
							} ?>
						</select>
						<?= form_error('status', '<small class="text-danger pl-3">', '</small>'); ?>
					</div>
					<div class="form-group">
						<input type="date" class="form-control" value="" id="tanggal" data-date-format="DD/MMM/YYYY" name="tanggal" placeholder="tanggal">
						<?= form_error('tanggal', '<small class="text-danger pl-3">', '</small>'); ?>
					</div>
					<div class="form-group">
						<label for="status">Status </label>
						<select class="form-control js-example-basic-single" style="width:100%" id="status" name="status">
							<option value=""></option>
							<option value="Izin">Izin</option>
							<option value="Alpha">Alpha</option>
							<option value="Sakit">Sakit</option>
						</select>
						<?= form_error('status', '<small class="text-danger pl-3">', '</small>'); ?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="Submit" class="btn btn-primary">Add</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Edit Modal -->
<?php foreach ($presensi as $p) : ?>
	<div class="modal fade" id="editPresensiModal<?= $p['id_presen'] ?>" tabindex="-1" role="dialog" aria-labelledby="editPresensiModal<?= $p['id_presen'] ?>Label" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="editMenuModal<?= $p['id_presen'] ?>">Edit Presensi</h5>
					<buttond type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</buttond>
				</div>
				<form action="<?= base_url('admin/editPresensi/' . $p['id_presen']); ?>" method="post">
					<div class="modal-body">
						<div class="form-group">
							<input type="text" class="form-control" readonly value="<?= $p['nama_depan'] . " " . $p['nama_belakang']; ?>" id="nama" name="nama" placeholder="Submenu title">
						</div>
						<div class="form-group">
							<input type="date" class="form-control" value="<?= date('Y-m-d', strtotime($p['tanggal'])); ?>" id="tanggal" name="tanggal" placeholder="Submenu url">
						</div>
						<div class="form-group">
							<select class="form-control js-example-basic-single status2" id="status2" name="status" style="width:100%">
								<option value="<?= $p['status'] ?>"><?= $p['status']; ?></option>
								<option value="Hadir">Hadir</option>
								<option value="Izin">Izin</option>
								<option value="Alpha">Alpha</option>
							</select>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php endforeach; ?>
<!-- End Edit Modal -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script language="JavaScript">
	$(document).ready(function() {
		var table1 = $('#dataTabless').DataTable({
			responsive: true
		});
		$('.js-example-basic-single').select2({
			placeholder: "Pilih Status",
			allowClear: true
		});
		$('#siswa').select2({
			placeholder: "Pilih Siswa",
			allowClear: true
		});
		$('#kelas').select2({
			placeholder: "Pilih Kelas",
			allowClear: true
		});
		$('#kelas2').select2({
			placeholder: "Pilih Kelas",
			allowClear: true
		});
		// $('#filter-siswa').select2({
		// 	placeholder: "Pilih Siswa",
		// 	allowClear: true,
		// });
		$('.filter').on('change', function() {
			let siswa = $("#filter-siswa").val()
			let kelas = $("#filter-kelas").val()
			console.log([siswa, kelas])
		});
		$('#status2').select2({});

		$("#tanggal").on("change", function() {
			this.setAttribute(
				"data-date",
				moment(this.value, "YYYY-MM-DD")
				.format(this.getAttribute("data-date-format"))
			)
		}).trigger("change");

		$('#tanggal').change(function() {
			alert(this.value.split("-").reverse().join("-"));
		});

		var span = document.getElementById('span');

		function time() {
			var d = new Date();
			var s = d.getSeconds();
			var m = d.getMinutes();
			var h = d.getHours();
			span.textContent = h + ":" + m + ":" + s;
		}

		setInterval(time, 1000);
	});
</script>
