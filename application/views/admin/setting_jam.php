<div class="container-fluid">

			<!-- Page Heading -->
			<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
			<span id="span" class="display-1 text-dark d-flex justify-content-center"></span>


			<div class="col-lg-6">
				<?= form_error('jamdatang', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
				<?= form_error('jampulang', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

				<?= $this->session->flashdata('message'); ?>
			
				<div class="box-body table-responsive">
					<a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newRoleModal">Tambah jam</a>


					<table class="table table-bordered table-striped" id="dataTables" width="100%">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Jam Datang</th>
								<th scope="col">Jam Pulang</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; ?>
							<?php foreach ($setting_jam as $jam) : ?>
								<tr>
									<th scope="row"><?= $i; ?></th>
									<td><?= date('H:i', strtotime($jam['jam_datang'])); ?></td>
									<td><?= date('H:i', strtotime($jam['jam_pulang'])); ?></td>
									<td>
										<a href="" data-toggle="modal" data-target="#editMenuModal<?= $jam['id'] ?>" class="badge badge-success">edit</a>
										<a href="<?= base_url('admin/deleteJam/').$jam['id'];?>" class="badge badge-danger"
										onclick="return confirm('Are you sure to delete this data ?');">delete</a>
									</td>
								</tr>
								<?php $i++; ?>
							<?php endforeach; ?>
						</tbody>
					</table>


				</div>

			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- End of Main Content -->


		<!-- Modal -->
		<div class="modal fade" id="newRoleModal" tabindex="-1" aria-labelledby="newRoleModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="newRoleModalLabel">Tambah Jam</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form action="<?= base_url('admin/setting_jam'); ?>" method="post">
						<div class="modal-body">
                        <label>Jam Datang</label>
							<div class="form-group">
								<input type="time" class="form-control" id="setting_jam" name="jamdatang" placeholder="Setting Jam">
                                <?= form_error('jamdatang', '<small class="text-danger pl-3">', '</small>')?>
							</div>
                            <label>Jam Pulang</label>
                            <div class="form-group">
								<input type="time" class="form-control" id="setting_jam" name="jampulang" placeholder="Setting Jam">
                                <?= form_error('jampulang', '<small class="text-danger pl-3">', '</small>')?>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="Submit" class="btn btn-primary">Add</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- edit Modal -->
		<?php foreach ($setting_jam as $r):?>
		<div class="modal fade" id="editMenuModal<?= $r['id']?>" tabindex="-1" setting_jam="dialog"
			aria-labelledby="editMenuModal<?= $jam['id']?>" aria-hidden="true">
			<div class="modal-dialog" setting="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="editMenuModal">Edit Jam</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form action="<?= base_url('admin/edit_jam/'.$jam['id']); ?>" method="post">
						<div class="modal-body">
                        <label>Jam Datang</label>
							<div class="form-group">
								<input type="time" class="form-control" value="<?= $jam['jam_datang'] ?>" id="role" name="jamdatang"
									placeholder="Jam Datang">
							</div>
                            <label>Jam Pulang</label>
							<div class="form-group">
								<input type="time" class="form-control" value="<?= $jam['jam_pulang'] ?>" id="role" name="jampulang"
									placeholder="Jam Pulang">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php endforeach;?>

		<script lang="text/javascript">
			var span = document.getElementById('span');

			function time() {
				var d = new Date();
				var s = d.getSeconds();
				var m = d.getMinutes();
				var h = d.getHours();
				span.textContent = h + ":" + m + ":" + s;
			}

			setInterval(time, 1000);

		</script>
