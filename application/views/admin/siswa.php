		<!-- Begin Page Content -->
		<style>
			.dataTables_filter {
				display: none;
			}
			#myImg:hover {
				opacity: 0.7;
			}
		</style>
		<div class="container-fluid">

			<!-- Page Heading -->
			<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
			<span id="span" class="display-1 text-dark d-flex justify-content-center"></span>

			<?= $this->session->flashdata('message'); ?>
			<div class="col-lg">
				<a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newMenuModal">Add presensi</a>
				<!-- <button class="btn btn-warning mb-2" id="refresh1">Refresh Page</button><br><br> -->
				<div class="box-body table-responsive">
					<!-- <button id="Reset">Reset</button> -->
					<table class="table table-bordered table-striped" id="dataTables" width="100%">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col" id="no_induk">No induk </th>
								<th scope="col" id="nama">Nama </th>
								<th scope="col" id="kelas" style="width: 10px;">Email </th>
								<th scope="col" id="tanggal" style="width: 120px;">Kelas</th>
								<th scope="col" style="width: 170px;">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; ?>
							<?php foreach ($siswa as $s) : ?>
								<tr>
									<th scope="row"><?= $i; ?></th>
									<td><?= $s['no_induk']; ?></td>
									<td><?= $s['nama_depan'] . " " . $s['nama_belakang']; ?></td>
									<td><?= $s['email']; ?></td>
									<td><?= $s['kelas']; ?></td>
									<td>
										<a href="" data-toggle="modal" data-target="#editPresensiModal<?= $s['id'] ?>" class="badge badge-success">edit</a>
										<a href="<?= base_url('admin/deleteSiswa/') . $s['id']; ?>" class="badge badge-danger" onclick="return confirm('Are you sure to delete this data ?');">delete</a>
									</td>
								</tr>
								<?php $i++; ?>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>

			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- End of Main Content -->

		<!-- Modal -->
		<div class="modal fade" id="newMenuModal" tabindex="-1" aria-labelledby="newMenuModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="newMenuModalLabel">Add Siswa</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form action="<?= base_url('admin/siswa'); ?>" method="POST">
						<div class="modal-body">
							<div class="form-group">
							<label for="nama_depan">Nama Depan</label>
								<input type="text" class="form-control" value="" id="nama_depan" name="nama_depan" placeholder="Masukkan Nama Depan">
								<?= form_error('nama_depan', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
							<div class="form-group">
							<label for="nama_depan">Nama Belakang</label>
								<input type="text" class="form-control" value="" id="nama_belakang" name="nama_belakang" placeholder="Masukkan Nama Belakang">
								<?= form_error('nama_belakang', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
							<div class="form-group">
							<label for="no_induk">Nomor Induk</label>
								<input type="text" class="form-control" value="" id="no_induk" name="no_induk" placeholder="Masukkan No Induk">
								<?= form_error('no_induk', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
							<div class="form-group">
							<label for="email">Email</label>
								<input type="email" class="form-control" value="" id="email" name="email" placeholder="Masukkan Email">
								<?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
							<div class="form-group">
							<label for="kelas">Kelas</label>
								<select class="form-control js-example-basic-single kelas" style="width:100%" id="kelas" name="kelas">
									<!-- <option value="" disabled>Pilih Kehadiran</option> -->
									<option value=""></option>
									<?php foreach ($kelas as $k) { ?>
										<option name="kelas" value="<?= $k['kelas']; ?>">
											<?= $k['kelas']; ?></option>
									<?php
									} ?>
								</select>
								<?= form_error('kelas', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
							<div class="form-group">
									<input type="password" class="form-control form-control-user" id="password1"
										name="password1" placeholder="Password">
									<?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
							<div class="form-group">
									
							<input type="password" class="form-control form-control-user" id="password2"
										name="password2" placeholder="Repeat Password">
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="Submit" class="btn btn-primary">Add</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- Edit Modal -->
		<?php foreach ($siswa as $s) : ?>
			<div class="modal fade" id="editPresensiModal<?= $s['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="editPresensiModal<?= $s['id'] ?>Label" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="editMenuModal<?= $s['id'] ?>">Edit Presensi</h5>
							<buttond type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</buttond>
						</div>
						<form action="<?= base_url('admin/editSiswa/' . $s['id']); ?>" method="post">
							<div class="modal-body">
								<div class="form-group">
								<label for="kelas">Nama Depan</label>
									<input type="text" class="form-control" value="<?= $s['nama_depan'];?>" id="nama_depan" name="nama_depan">
									<?= form_error('nama_depan', '<small class="text-danger pl-3">', '</small>'); ?>
								</div>
								<div class="form-group">
								<label for="kelas">Nama Belakang</label>
									<input type="text" class="form-control" value="<?=$s['nama_belakang']; ?>" id="nama_belakang" name="nama_belakang">
									<?= form_error('nama_belakang', '<small class="text-danger pl-3">', '</small>'); ?>
								</div>
								<div class="form-group">
								<label for="kelas">Email</label>
									<input type="email" class="form-control" value="<?=$s['email']; ?>" id="email" name="email">
									<?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
								</div>
								<div class="form-group">
								<label for="kelas">No Induk</label>
									<input type="text" class="form-control" value="<?=$s['no_induk']; ?>" id="no_induk" name="no_induk">
									<?= form_error('no_induk', '<small class="text-danger pl-3">', '</small>'); ?>
								</div>
								<div class="form-group">
								<label for="kelas">Kelas</label>
								<select class="form-control js-example-basic-single kelas" style="width:100%" id="kelas" name="kelas">
									<!-- <option value="" disabled>Pilih Kehadiran</option> -->
									<option value="<?= $s['kelas'] ?>" selected><?= $s['kelas'] ?></option>
									<?php foreach ($kelas as $k) { ?>
										<option name="kelas" value="<?= $k['kelas']; ?>">
											<?= $k['kelas']; ?></option>
									<?php
									} ?>
								</select>
								<?= form_error('kelas', '<small class="text-danger pl-3">', '</small>'); ?>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
								<button type="submit" class="btn btn-primary">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>

		<?php endforeach; ?>
		<!-- End Edit Modal -->
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
		<script language="JavaScript">
			$(document).ready(function() {
				$("#refresh1").click(function () {
					window.location.href = window.location.href
				});
				var table1 = $('#dataTabless').DataTable({
					responsive: true
				});
				$('.js-example-basic-single').select2({
					placeholder: "Pilih Status",
					allowClear: true
				});
				$('#siswa').select2({
					placeholder: "Pilih Siswa",
					allowClear: true
				});
				$('#kelas').select2({
					placeholder: "Pilih Kelas",
					allowClear: true
				});
				$('.filter').on('change', function() {
					let siswa = $("#filter-siswa").val()
					let kelas = $("#filter-kelas").val()
					console.log([siswa, kelas])
				});
				$('#status2').select2({});

				$("#tanggal").on("change", function() {
					this.setAttribute(
						"data-date",
						moment(this.value, "YYYY-MM-DD")
						.format(this.getAttribute("data-date-format"))
					)
				}).trigger("change");

				$('#tanggal').change(function() {
					alert(this.value.split("-").reverse().join("-"));
				});

				var span = document.getElementById('span');

				function time() {
					var d = new Date();
					var s = d.getSeconds();
					var m = d.getMinutes();
					var h = d.getHours();
					span.textContent = h + ":" + m + ":" + s;
				}

				setInterval(time, 1000);

				$('#no_induk').each(function() {
					var title = $(this).text();
					$(this).html('<input type="text" placeholder="Cari ' + title + '" style="width: 110px;" />');
				});
				$('#nama').each(function() {
					var title = $(this).text();
					$(this).html('<select class="form-control js-example-basic-single siswa" style="width:125px;"  id="selectt" name="siswa"><option value=""></option><?php foreach ($daftar_user as $du) { ?><option name="siswa" value="<?= $du['nama_depan'] . " " . $du['nama_belakang']; ?>"><?= $du['nama_depan'] . " " . $du['nama_belakang']; ?></option><?php } ?></select>');
				});
				$('#selectt').select2({
					placeholder: "Pilih Siswa",
					allowClear: true
				});
				$('#kelas').each(function() {
					var title = $(this).text();
					$(this).html('<select class="form-control js-example-basic-single kelas" style="width:107px;"  id="selectkelas" name="kelas"><option value=""></option><?php foreach ($kelas as $k) { ?><option name="kelas" value="<?= $k['kelas']; ?>"><?= $k['kelas']; ?></option><?php } ?></select>');
				});
				$('#selectkelas').select2({
					placeholder: "Pilih Kelas",
					allowClear: true
				});
				// $('#tanggal').each(function () {
				// 	var title = $(this).text();
				// 	$(this).html('<input type="date" dateformat="d M y" class="clearable" placeholder="Cari ' + title +
				// 		'" style="width: 120px;"/>');
				// });
				$('#status').each(function() {
					var title = $(this).text();
					$(this).html('<input type="text" placeholder="Cari ' + title + '" style="width: 86px;"/>');
				});

				table1.columns().every(function() {
					var that = this;

					$('select', this.header()).on('change', function() {
						if (that.search() !== this.value) {
							that
								.search(this.value)
								.draw();
						}
					});

					$('input', this.header()).on('keyup change', function() {
						if (that.search() !== this.value) {
							that
								.search(this.value)
								.draw();
						}
					});
				});
			});
		</script>
