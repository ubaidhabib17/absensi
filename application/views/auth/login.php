	<div class="container">

		<!-- Outer Row -->
		<div class="row justify-content-center">

			<div class="col-lg-7">

				<div class="card o-hidden border-0 shadow-lg my-5">
					<div class="card-body p-0">
						<!-- Nested Row within Card Body -->
						<div class="row">
							<div class="col-lg">
								<div class="p-5">
									<div class="text-center">
										<h1 class="h4 text-gray-900 mb-4">Login</h1>
									</div>

									<?= $this->session->flashdata('message'); ?>

									<form class="user" method="post" action="<?= base_url('auth'); ?>">
										<div class="form-group">
											<input type="text" class="form-control form-control-user" id="email" name="email" placeholder="Enter Email Address..." value="<?= set_value('email'); ?>">
											<?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
										</div>
										<div class="form-group">
											<input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Password" value="<?= set_value('password'); ?>">
											<?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
										</div>
										<!--<div class="form-group row">
											<label for="email" class="col-sm-3 col-form-label">Pertanyaan</label>
											<div class="col-sm-9">
												<select name="pertanyaan" id="pertanyaaan" class="form-control">
													<option selected>Pilih Pertanyaan Dibawah Ini !</option>
													<?php foreach ($pertanyaan as $p) { ?>
													<option value="<?= $p['pertanyaan'] ?>"><?= $p['pertanyaan']; ?></option>
													<?php
													} ?>
												</select>
												<?= form_error('pertanyaan', '<small class="text-danger pl-3">', '</small>'); ?>
											</div>
										</div>
										<div class="form-group row">
											<label for="jawaban" class="col-sm-3 col-form-label">Jawaban</label>
											<div class="col-sm-9">
												<input type="text" class="form-control" id="jawaban" name="jawaban"
													autocomplete="off">
												<?= form_error('jawaban', '<small class="text-danger pl-3">', '</small>'); ?>
											</div>
										</div>-->
										<div class="form-group">
											<div class="g-recaptcha" data-type="image" data-sitekey="6Lex8TYaAAAAAOBWxb79-ZZi4zz_HLhOnnWgdqIo"></div>
											<?= form_error('g-recaptcha-response', '<small class="text-danger pl-3">', '</small>'); ?>
										</div>
										<button type="submit" class="btn btn-primary btn-user btn-block">
											Login
										</button>
									</form>
									<div class="text-center">
										<a class="small" href="<?= base_url('auth/forgotpassword'); ?>">Forgot
											Password?</a>
									</div>
									<div class="text-center">
										<a class="small" href="<?= base_url('auth/registration'); ?>">Create an
											Account!</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>

	</div>
