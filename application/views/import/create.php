<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <span id="span" class="display-1 text-dark d-flex justify-content-center"></span>

    <?= $this->session->flashdata('message'); ?>
    <div class="col-lg">
        <!-- <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newMenuModal">Add presensi</a> -->
        <?= form_error('presensi', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?php if (validation_errors()) : ?>
            <div class="alert alert-danger" role="alert">
                <?= validation_errors(); ?>
            </div>
        <?php endif; ?>
    </div>

    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<div class="container mt-3">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Upload File Excel
                </div>
                <form method="POST" action="<?= site_url('admin/excel') ?>" enctype="multipart/form-data">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label class="col-form-label text-md-left">Upload File</label>
                                        <input type="file" class="form-control" name="file" accept=".xls, .xlsx" required>
                                        <div class="mt-1">
                                            <span class="text-secondary">File yang harus diupload : .xls, xlsx</span>
                                        </div>
                                        <?= form_error('file', '<div class="text-danger">', '</div>') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <div class="form-group mb-0">
                            <button type="submit" name="import" class="btn btn-primary"><i class="fas fa-upload mr-1"></i>Upload</button>
                        </div>
                    </div>
                </form>
            </div>