		

		
				<!-- Begin Page Content -->
				<div class="container-fluid">

					<!-- Page Heading -->
					<h1 class="h3 mb-4 text-gray-800"><?= $title ;?></h1>
					<span id="span" class="display-1 text-dark d-flex justify-content-center"></span>

					<div class="row">
						<div class="col-lg-6">
							<?= $this->session->flashdata('message'); ?>
						</div>
					</div>

					<div class="card mb-3" style="max-width: 540px;">
						<div class="row no-gutters">
							<div class="col-md-4">
								<img src="https://image.flaticon.com/icons/png/512/104/104784.png" class="card-img" alt="...">
							</div>
							<div class="col-md-8">
								<div class="card-body">
									<h5 class="card-title"><?= $user['nama_depan']. " " .$user['nama_belakang']; ?></h5>
									<p class="card-text"><?= $user['email']; ?></p>
									<p class="card-text"><small class="text-muted">Member since  <?= date('d F Y', strtotime($user['date_created']));?></small></p>
								</div>
							</div>
						</div>
					</div>

				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- End of Main Content -->

			<script lang="text/javascript">
			var span = document.getElementById('span');

			function time() {
				var d = new Date();
				var s = d.getSeconds();
				var m = d.getMinutes();
				var h = d.getHours();
				span.textContent = h + ":" + m + ":" + s;
			}

			setInterval(time, 1000);

		</script>

			