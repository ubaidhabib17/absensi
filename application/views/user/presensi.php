		<!-- Begin Page Content -->
		<style>
			#btnLokasi,
			#latitude,
			#longitude {
				display: none;
			}

			#mapid {
				height: 180px;
			}

		</style>
		<div class="container-fluid">

			<!-- Page Heading -->
			<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

			<div class="row">
				<div class="col-lg-8">
					<?= form_error('presensi', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
					<?php if (validation_errors()) : ?>
					<div class="alert alert-danger" role="alert">
						<?= validation_errors(); ?>
					</div>
					<?php endif; ?>
					<?= $this->session->flashdata('message'); ?>
					<form action="<?= base_url('user/presensi') ?>" method="POST" id="presensi">
						<!-- <div class="form-group row">
							<label for="email" class="col-sm-2 col-form-label">Pertanyaan</label>
							<div class="col-sm-10">
								<select name="pertanyaan" id="pertanyaaan" class="form-control">
									<option selected>Pilih Pertanyaan Dibawah Ini !</option>
									<?php foreach ($pertanyaan as $p) { ?>
									<option value="<?= $p['pertanyaan'] ?>"><?= $p['pertanyaan']; ?></option>
									<?php
									} ?>
								</select>
								<?= form_error('pertanyaan', '<small class="text-danger pl-3">', '</small>'); ?>
							</div>
						</div> -->
						<div class="form-group row">
							<!-- <label for="jawaban" class="col-sm-2 col-form-label">Jawaban</label> -->
							<div class="col-sm-10">
								<button onclick="getLocation()" id="btnLokasi" type="button" name="lokasi">Lihat
									Lokasi</button>
								<input type='text' name='latitude' id="latitude" />
								<input type='text' name='longitude' id="longitude" />
								<!-- <input type="text" class="form-control" id="jawaban" name="jawaban" autocomplete="off"> -->
								<!-- <?= form_error('jawaban', '<small class="text-danger pl-3">', '</small>'); ?> -->

								<input type="hidden" class="form-control form-control-user" id="email" name="email"
									value="<?= $user['email']; ?>">
								<?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>

								<input type="hidden" class="form-control form-control-user" id="id_user" name="id_user"
									value="<?= $user['id']; ?>">
								<?= form_error('id_user', '<small class="text-danger pl-3">', '</small>'); ?>

								<input type="hidden" name="foto" id="foto">
							</div>
						</div>
						<!-- <div class="form-group row">
							<label for="nama_belakang" class="col-sm-2 col-form-label">Status</label>
							<div class="col-sm-10">
								<div class="radio mt-2">
									<label><input type="radio" name="status" class="ml-2" value="Hadir"> Hadir</label>
									<label><input type="radio" name="status" class="ml-5" value="Izin"> Izin</label>
									<label><input type="radio" name="status" class="ml-5" value="Alfa"> Alfa</label>
								</div>
							</div>
						</div> -->
						<!-- <div id="my_camera"></div>
						<input type="hidden" id="image" name="image" value="">
						<input type=button value="Take Snapshot" onClick="take_snapshot()"> -->
						<!-- <input type=button value="Save Snapshot" onClick="saveSnap()"> -->
						<!-- <div id="results"></div> -->

						<!-- <h6 class=" text-dark d-flex ">Silahkan absen, Klik tombol di bawah ini!</h6> -->
						<div class="form-group row">
							<div class="col-sm-10">
								<!-- <p class="lead">NB : Wajib direload setelah submit gagal !</p> -->
								<span>Sebelum absen, pastikan Longitude, Latitude dan Foto muncul dahulu !</span><br><br>
								<button type="submit" class="btn btn-primary">Absen</button>
								<!-- <button type="submit" onclick="document.location.reload(true)"
									class="btn btn-warning">Reload</button> -->
							</div>	
							<div class="col-lg-4 mt-2">
								<label for="" class="h5">Hasil Foto</label>
								<div id="results"></div>
							</div>
							<br><br>
							<br>
						</div>
						<!-- <div class="form-group">
							<div class="col-sm-4">
								<span>Pilih dari tanggal</span>
								<div class="form-group row">
									<input type="text" class="form-control pickdate date_range_filter" name="startdate"
										id="startdate">
									<span class="input-group-addon" id="basic-addon2"><span
											class="glyphicon glyphicon-calendar"></span></span>
								</div>
								<span>Sampai tanggal</span>
								<div class="form-group row">
									<input type="text" class="form-control pickdate date_range_filter2" name="">
									<span class="input-group-addon" id="basic-addon2"><span
											class="glyphicon glyphicon-calendar"></span></span>
								</div>
							</div>
						</div> -->
					</form>
					<br><br>
					<p>Lokasi Anda :</p>
					<div id="dataLokasi">
					</div><br><br>
					<div id="my_camera"></div>
					<input type=button class="mt-2" value="Take Snapshot" onClick="take_snapshot()">
					<!-- <input type=button value="Save Snapshot" onClick="saveSnap()"> -->
					<!-- <iframe name="iframe1" src="http://www.google.com/custom?q=&btnG=Search"></iframe> -->
					<!-- <a href="https://futsalzoneid.com/" target="iframe1">link</a> -->
					<!-- <div id="show_maps" style="width:100%;height:100%;"></div> -->
					<!-- <div id="mapid"></div> -->
					<!-- <form action="<?= base_url('user/tampilPeta') ?>" method="POST">
						<div class="form-group">
						  <label for="">Longitude</label>
						  <input type="text" class="form-control" name="longitude1" id="longitude1" aria-describedby="helpId" placeholder="Longitude">
						</div>
						<div class="form-group">
						  <label for="">Latitude</label>
						  <input type="text" class="form-control" name="latitude1" id="latitude1" aria-describedby="helpId" placeholder="Latitude">
						</div>
						<button type="submit">Kirim</button>
					</form>
					<a href="https://www.google.com/maps/search/?api=1&query=<?= $latitude ;?>,<?= $longitude ?>">Menuju Maps</a> -->
					<!-- <form action="<?= base_url('user/riwayat_presensi') ?>" method="POST" id="cari">
						<div class="form-group row">
							<div class="col-sm-10">
								<input type="date" class="form-contorl" name="startdate" placeholder="Start Date">
								<input type="date" class="form-contorl" name="enddate" placeholder="End Date">
								<button type="submit" class="btn btn-primary" id="cari" name="cari">Cari</button>
							</div>
						</div>
					</form> -->
					<!-- <table class="table table-bordered table-striped" id="dataTables" width="100%">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Nama </th>
								<th scope="col">Tanggal</th>
								<th scope="col">Status</th>
								<th scope="col">Kelas</th>
							</tr>
						</thead>

						<tbody>
							<?php $i = 1; ?>
							<?php foreach ($presensi as $p) : ?>
							<tr>
								<th scope="row"><?= $i; ?></th>
								<td><?= $p['nama_depan'] . " " . $p['nama_belakang']; ?></td>
								<td><?= $p['tanggal']; ?></td>
								<td><?= $p['status']; ?></td>
								<td><?= $p['kelas']; ?></td>
							</tr>
							<?php $i++; ?>
							<?php endforeach; ?>
						</tbody>
					</table> -->
				</div>
			</div>
		</div>
		<!-- /.container-fluid -->

		</div>
		<!-- End of Main Content -->
		<!-- Webcam.min.js -->

		<!-- Configure a few settings and attach camera -->
		<script language="JavaScript">


		</script>

		<script type="text/javascript" src="<?= base_url('assets/'); ?>js/webcamjs/webcam.js"></script>

		<script lang="JavaScript">
			var x = document.getElementById("dataLokasi");
			// var mymap = L.map('mapid').setView([51.505, -0.09], 13);

			// L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
			// 	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
			// 	maxZoom: 18,
			// 	id: 'mapbox/streets-v11',
			// 	tileSize: 512,
			// 	zoomOffset: -1,
			// 	accessToken: 'your.mapbox.access.token'
			// }).addTo(mymap);

			// var marker = L.marker([51.5, -0.09]).addTo(mymap);

			// var circle = L.circle([51.508, -0.11], {
			// 	color: 'red',
			// 	fillColor: '#f03',
			// 	fillOpacity: 0.5,
			// 	radius: 500
			// }).addTo(mymap);

			function getLocation() {
				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(showPosition);
				} else {
					console.log("geolocation tidak di aktifkan");
					alert('Geolocation is not supported by this browser');
					x.innerHTML = "Geolocation is not supported by this browser.";
				}
			}

			function showPosition(position) {
				x.innerHTML = "Latitude: " + position.coords.latitude +
					"<br>Longitude: " + position.coords.longitude;
				document.getElementById("latitude").value = position.coords.latitude;
				document.getElementById("longitude").value = position.coords.longitude;
			}

			window.onload = function () {
				document.getElementById('btnLokasi').click();
			}

			Webcam.set({
				width: 320,
				height: 240,
				image_format: 'jpeg',
				flip_horiz: true,
				enable_flash: true,
				jpeg_quality: 90
			});
			Webcam.attach('#my_camera');

			// <!--Code to handle taking the snapshot and displaying it locally-- >
			function take_snapshot() {

				// take snapshot and get image data
				Webcam.snap(function (data_uri) {
					$("#foto").val(data_uri);
					// display results in page
					document.getElementById('results').innerHTML =
						'<img id="imageprev" src="' + data_uri + '"/>';
				});
			}

			function saveSnap() {
				// Get base64 value from <img id='imageprev'> source
				var base64image = document.getElementById("imageprev").src;

				Webcam.upload(base64image, 'User.php', function (code, text) {
					console.log('Save successfully');
					//console.log(text);
				});

			}


			// function initMap() {
			// 	var map = new google.maps.Map(document.getElementById('show_maps'), {
			// 		center: {
			// 			lat: -1.035721,
			// 			lng: 118.436931
			// 		},
			// 		zoom: 5
			// 	});
			// }

		</script>
