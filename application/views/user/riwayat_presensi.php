		<!-- Begin Page Content -->
		<style>
			#myImg:hover {
				opacity: 0.7;
			}
		</style>
		<div class="container-fluid">

			<!-- Page Heading -->
			<h1 class="h3 mb-4 text-gray-800"><?= $title ;?></h1>
			<span id="span" class="display-1 text-dark d-flex justify-content-center"></span>
			<div class="row">
				<div class="col-lg-8">
					<?= form_error('presensi', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
					<?php if (validation_errors()) : ?>
					<div class="alert alert-danger" role="alert">
						<?= validation_errors(); ?>
					</div>
					<?php endif; ?>
					<?= $this->session->flashdata('message'); ?>
					<!-- <form action="<?= base_url('user/riwayat_presensi')?>" method="POST" id="cari">
						<div class="form-group row">
							<div class="col-sm-10">
								<input type="date" class="form-contorl" name="startdate" placeholder="Start Date">
								<input type="date" class="form-contorl" name="enddate" placeholder="End Date">
								<button type="submit" class="btn btn-primary" id="cari" name="cari">Cari</button>
							</div>
						</div>
					</form> -->
					<form action="<?= base_url('user/riwayat_presensi')?>" method="POST" id="cari">
						<div class="form-group row">
							<div class="col-sm-3">
								<label for="stardate">Mulai Tanggal</label>
								<!-- <?= form_error('startdate', '<small class="text-danger pl-3">', '</small>'); ?> -->
								<input type="date" class="form-control" name="startdate" placeholder="Start Date">
							</div>
							<div class="col-sm-3">
								<label for="enddate">Sampai Tanggal</label>
								<!-- <?= form_error('enddate', '<small class="text-danger pl-3">', '</small>'); ?> -->
								<input type="date" class="form-control" name="enddate" placeholder="End Date">
							</div>
							<div class="col-sm-3">
								<br><button type="submit" class="btn btn-primary mt-2" id="cari" name="cari">Cari</button>
							</div>
						</div>
					</form>
					<button class="btn btn-warning mb-2" id="refresh">Refresh Page</button>
					<table class="table table-bordered table-striped" id="dataTables" width="100%">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Nama </th>
								<th scope="col">Tanggal</th>
								<th scope="col">Status</th>
								<th scope="col">Kelas</th>
								<th scope="col" style="width: 100px">Foto</th>
								<!-- <th scope="col">Longitude</th>
								<th scope="col">Latitude</th> -->
							</tr>
						</thead>

						<tbody>
							<?php $i = 1; ?>
							<?php foreach ($presensi as $p) : ?>
							<tr>
								<th scope="row"><?= $i; ?></th>
								<td><?= $p['nama_depan']. " " . $p['nama_belakang'];?></td>
								<td><?= date('d-m-Y', strtotime($p['tanggal'])); ?></td>
								<td><?= $p['status']; ?></td>
								<td><?= $p['kelas']; ?></td>
								<td>
									<a href="" data-toggle="modal" data-target="#exampleModal<?= $p['id_presen'] ?>">
										<img src="<?= base_url(); ?>assets/foto/<?= $p['image'] ?>" alt="" id="myImg" data-toggle="modal" data-target="#exampleModal<?= $p['id_presen'] ?>" style="width: 100%;">
									</a>
								</td>
								<!-- <td><?= $p['latitude']; ?></td>
								<td><?= $p['longitude']; ?></td> -->
							</tr>
							<?php $i++; ?>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<?php foreach ($presensi as $p) : ?>
		<!-- Modal  Foto-->
		<div class="modal fade" id="exampleModal<?= $p['id_presen'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Foto Siswa</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<label for="" class="h5">Nama = <?= $p['nama_depan'] . " " . $p['nama_belakang']; ?></label><br>
							<label for="" class="h5">Kelas = <?= $p['kelas']; ?></label>
							<img src="<?= base_url(); ?>assets/foto/<?= $p['image'] ?>" alt="foto siswa" id="imagepreview" style="width: 100%; height: 100%;">
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		<!-- /.container-fluid -->
		<?php endforeach; ?>

		<script lang="text/javascript">
			var span = document.getElementById('span');

			function time() {
				var d = new Date();
				var s = d.getSeconds();
				var m = d.getMinutes();
				var h = d.getHours();
				span.textContent = h + ":" + m + ":" + s;
			}

			setInterval(time, 1000);

		</script>


		<!-- End of Main Content -->
		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
			integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous">
		</script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
			integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous">
		</script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.js"></script>
		<script language="JavaScript">
			Webcam.set({
				width: 320,
				height: 240,
				image_format: 'jpeg',
				jpeg_quality: 90
			});
			Webcam.attach('#my_camera');

			function take_snapshot() {
				// take snapshot and get image data
				Webcam.snap( function(data_uri) {
					var raw_image_data = data_uri.replace(/^data\:image\/w+\;base64\,/, '');

					document.getElementById('image').value = raw_image_data;
					document.getElementById('presensi').submit();
				// display results in page
				document.getElementById('results').innerHTML = 
				'<img id="imageprev" src="'+data_uri+'"/>';
				} );
			}

			function saveSnap(){
				// Get base64 value from <img id='imageprev'> source
				var base64image = document.getElementById("imageprev").src;

				Webcam.upload( base64image, 'User.php', function(code, text) {
				console.log('Save successfully');
				//console.log(text);
				});

			}

		</script> -->
